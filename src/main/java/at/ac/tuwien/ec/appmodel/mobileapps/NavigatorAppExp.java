package at.ac.tuwien.ec.appmodel.mobileapps;

import static java.util.Arrays.asList;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import at.ac.tuwien.ec.appmodel.MobileApplication;
import at.ac.tuwien.ec.edgeoffload.SimulationConstants;
import at.ac.tuwien.ec.utils.ExponentialDistributionGenerator;
import di.unipi.socc.fogtorchpi.application.SoftwareComponent;
import di.unipi.socc.fogtorchpi.utils.Couple;
import di.unipi.socc.fogtorchpi.utils.Hardware;
import di.unipi.socc.fogtorchpi.utils.QoSProfile;

public class NavigatorAppExp extends MobileApplication {

	public double data_variance = 1e+3;
	
	double config_panel_mips = 1.0;
    double gps_mips = 1.0;
    double control_mips = 1.0/2.0;
    double maps_mips = 1.0/3.0;
    double path_calc_mips = 1.0/5.0;
    double traffic_mips = 1.0/5.0;
    double voice_synth_mips = 1.0/2.0;
    double gui_mips = 0.5;
    double speed_mips = 0.5;
    
    double maps_size = SimulationConstants.maps_size;
	
	public NavigatorAppExp(){
        super(0);
		sampleTasks();
        sampleLinks();
	}

	public NavigatorAppExp(int workload_id) {
		super(workload_id);
		sampleTasks();
		sampleLinks();
	}

	public void sampleLinks() {
		addLink("CONF_PANEL"+"_"+getWorkloadId(), "GPS"+"_"+getWorkloadId(), 60, 0.1);
        addLink("CONF_PANEL"+"_"+getWorkloadId(), "CONTROL"+"_"+getWorkloadId(), 60, 0.1);
        addLink("GPS"+"_"+getWorkloadId(), "CONTROL"+"_"+getWorkloadId(), 30, 0.1);
        addLink("CONTROL"+"_"+getWorkloadId(), "MAPS"+"_"+getWorkloadId(), 30, 0.1);
        addLink("CONTROL"+"_"+getWorkloadId(), "PATH_CALC"+"_"+getWorkloadId(), 30, 0.1, 0.1);
        addLink("CONTROL"+"_"+getWorkloadId(), "TRAFFIC"+"_"+getWorkloadId(), 60, 0.1, 0.1);
        addLink("MAPS"+"_"+getWorkloadId(), "PATH_CALC"+"_"+getWorkloadId(), 30, 0.1, 0.1);
        addLink("TRAFFIC"+"_"+getWorkloadId(), "PATH_CALC"+"_"+getWorkloadId(), 60, 0.1, 0.1);
        addLink("PATH_CALC"+"_"+getWorkloadId(), "VOICE_SYNTH"+"_"+getWorkloadId(), 60, 0.1, 0.1);
        addLink("PATH_CALC"+"_"+getWorkloadId(), "GUI"+"_"+getWorkloadId(), 60, 0.1, 0.1);
        addLink("PATH_CALC"+"_"+getWorkloadId(), "SPEED_TRAP"+"_"+getWorkloadId(), 30, 0.1, 0.1);
	}

	public void sampleTasks() {
		double data_size = ExponentialDistributionGenerator.getNext(maps_size);
		addComponent("CONF_PANEL"+"_"+getWorkloadId(), asList("python")
				,new Hardware(1, 1, 1)
				,false
				,Math.floor(ExponentialDistributionGenerator.getNext(config_panel_mips))
        		,5e+3
        		,5e+3);
        addComponent("GPS"+"_"+getWorkloadId(), asList("python")
        		,new Hardware(1, 3, 1)
        		,false
        		,Math.floor(ExponentialDistributionGenerator.getNext(gps_mips))
        		,5e+3
        		,5e+3);
        addComponent("CONTROL"+"_"+getWorkloadId(), asList("python")
        		,new Hardware(2, 3, 1)
        		,Math.floor(ExponentialDistributionGenerator.getNext(control_mips))
        		,5e+3
        		,5e+3);
        addComponent("MAPS"+"_"+getWorkloadId(), asList("python")
        		,new Hardware(2, 5, 5)
        		,Math.floor(ExponentialDistributionGenerator.getNext(maps_mips))
        		,5e3
        		,data_size);
        addComponent("PATH_CALC"+"_"+getWorkloadId(), asList("python")
        		,new Hardware(1, 2, 10)
        		,Math.floor(ExponentialDistributionGenerator.getNext(path_calc_mips))
        		,data_size
        		,data_size);
        addComponent("TRAFFIC"+"_"+getWorkloadId(), asList("python")
        		,new Hardware(1, 1, 1)
        		,Math.floor(ExponentialDistributionGenerator.getNext(traffic_mips))
        		,data_size
        		,data_size);
        addComponent("VOICE_SYNTH"+"_"+getWorkloadId(), asList("python")
        		,new Hardware(1, 1, 1)
        		,false
        		,Math.floor(ExponentialDistributionGenerator.getNext(voice_synth_mips))
        		,1e+3
        		,2e+3);
        addComponent("GUI"+"_"+getWorkloadId(), asList("python")
        		,new Hardware(1, 1, 1)
        		,false
        		,Math.floor(ExponentialDistributionGenerator.getNext(gui_mips))
        		,10e+3
        		,1e+3);
        addComponent("SPEED_TRAP"+"_"+getWorkloadId(), asList("python")
        		,new Hardware(1, 1, 1)
        		,false
        		,Math.floor(ExponentialDistributionGenerator.getNext(speed_mips))
        		,10e+3
        		,1e+3);
	}
	
	
	@Override
	public void sample() {
		S.clear();
		L.clear();
		sampleTasks();
		sampleLinks();
	}

	@Override
	public MobileApplication clone() {
		NavigatorAppExp cloned = new NavigatorAppExp(this.workload_id);
		cloned.S.clear();
		cloned.L.clear();
		cloned.S = (ArrayList<SoftwareComponent>) this.S.clone();
		cloned.L = (HashMap<Couple, QoSProfile>) this.L.clone();
		return cloned;
	}

}
