package at.ac.tuwien.ec.appmodel.mobileapps;

import static java.util.Arrays.asList;

import java.util.ArrayList;
import java.util.HashMap;

import at.ac.tuwien.ec.appmodel.MobileApplication;
import at.ac.tuwien.ec.edgeoffload.SimulationConstants;
import at.ac.tuwien.ec.utils.ExponentialDistributionGenerator;
import di.unipi.socc.fogtorchpi.application.SoftwareComponent;
import di.unipi.socc.fogtorchpi.utils.Couple;
import di.unipi.socc.fogtorchpi.utils.Hardware;
import di.unipi.socc.fogtorchpi.utils.QoSProfile;

public class FacebookApp extends MobileApplication {

	public FacebookApp(){
		super();
		sampleTasks();
		sampleLinks();
	}
	
	public FacebookApp(int workload_id) {
		super(workload_id);
		sampleTasks();
		sampleLinks();
	}

	@Override
	public MobileApplication clone() {
		FacebookApp cloned = new FacebookApp(this.workload_id);
		cloned.S.clear();
		cloned.L.clear();
		cloned.S = (ArrayList<SoftwareComponent>) this.S.clone();
		cloned.L = (HashMap<Couple, QoSProfile>) this.L.clone();
		return cloned;
	}

	@Override
	public void sampleLinks() {
		addLink("FACEBOOK_GUI"+"_"+getWorkloadId(), "GET_TOKEN"+"_"+getWorkloadId(), 60, 0.1);
		//addLink("FACEBOOK_GUI"+"_"+getWorkloadId(), "POST_REQUEST"+"_"+getWorkloadId(), Integer.MAX_VALUE, Double.MIN_VALUE);
		addLink("GET_TOKEN"+"_"+getWorkloadId(), "POST_REQUEST"+"_"+getWorkloadId(), 60, 0.1);
		addLink("POST_REQUEST"+"_"+getWorkloadId(), "PROCESS_RESPONSE"+"_"+getWorkloadId(), 60, 0.1);
		//addLink("POST_REQUEST"+"_"+getWorkloadId(), "FILE_UPLOAD"+"_"+getWorkloadId(), Integer.MAX_VALUE, Double.MIN_VALUE);
		addLink("PROCESS_RESPONSE"+"_"+getWorkloadId(), "FILE_UPLOAD"+"_"+getWorkloadId(),60, 0.1);
		addLink("FILE_UPLOAD"+"_"+getWorkloadId(), "APPLY_FILTER"+"_"+getWorkloadId(), 60, 0.1);
		addLink("APPLY_FILTER"+"_"+getWorkloadId(), "FACEBOOK_POST"+"_"+getWorkloadId(), 60, 0.1);
	}

	@Override
	public void sampleTasks() {
		double img_size = ExponentialDistributionGenerator.getNext(SimulationConstants.image_size);
		//double img_size = 1e+3;
		addComponent("FACEBOOK_GUI"+"_"+getWorkloadId(), asList("python"),
				new Hardware(1, 1, 1)
				,false
				,ExponentialDistributionGenerator.getNext(2e-1)
        		,5e+3
        		,1e+3);
		addComponent("GET_TOKEN"+"_"+getWorkloadId(), asList("python"),
				new Hardware(1, 1, 1)
				,ExponentialDistributionGenerator.getNext(2e-1)
        		,1e+3
        		,1e+3);
		addComponent("POST_REQUEST"+"_"+getWorkloadId(), asList("python"),
				new Hardware(1, 1, 1)
				,ExponentialDistributionGenerator.getNext(2e-1)
        		,1e+3
        		,5e+3);
		addComponent("PROCESS_RESPONSE"+"_"+getWorkloadId(), asList("python"),
				new Hardware(1, 1, 1)
				,ExponentialDistributionGenerator.getNext(2e-1)
        		,1e+3
        		,1e+3);
		addComponent("FILE_UPLOAD"+"_"+getWorkloadId(), asList("python"),
				new Hardware(1, 1, 1)
				,false
				,ExponentialDistributionGenerator.getNext(2e-1)
        		,img_size
        		,img_size);
		addComponent("APPLY_FILTER"+"_"+getWorkloadId(), asList("python"),
				new Hardware(1, 1, 1)
				,ExponentialDistributionGenerator.getNext(2e-1)
        		,img_size
        		,img_size);
		addComponent("FACEBOOK_POST"+"_"+getWorkloadId(), asList("python"),
				new Hardware(1, 1, 1)
				,false
				,ExponentialDistributionGenerator.getNext(2e-1)
        		,img_size
        		,5e+3);
	}

}
