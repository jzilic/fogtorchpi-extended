package at.ac.tuwien.ec.appmodel.mobileapps;

import static java.util.Arrays.asList;

import java.util.ArrayList;
import java.util.HashMap;

import at.ac.tuwien.ec.appmodel.MobileApplication;
import at.ac.tuwien.ec.edgeoffload.SimulationConstants;
import at.ac.tuwien.ec.utils.ExponentialDistributionGenerator;
import di.unipi.socc.fogtorchpi.application.SoftwareComponent;
import di.unipi.socc.fogtorchpi.utils.Couple;
import di.unipi.socc.fogtorchpi.utils.Hardware;
import di.unipi.socc.fogtorchpi.utils.QoSProfile;

public class FaceRecognizerApp extends MobileApplication{

	public FaceRecognizerApp() {
		super(0);
		sampleTasks();
		sampleLinks();
	}
	
	public FaceRecognizerApp(int id) {
		super(id);
		sampleTasks();
		sampleLinks();
	}
	
	@Override
	public MobileApplication clone() {
		FaceRecognizerApp cloned = new FaceRecognizerApp(this.workload_id);
		cloned.S.clear();
		cloned.L.clear();
		cloned.S = (ArrayList<SoftwareComponent>) this.S.clone();
		cloned.L = (HashMap<Couple, QoSProfile>) this.L.clone();
		return cloned;
	}

	@Override
	public void sampleLinks() {
		addLink("FACERECOGNIZER_UI"+"_"+getWorkloadId(), "FIND_MATCH"+"_"+getWorkloadId(), 60, 0.1);
		addLink("FIND_MATCH_INIT"+"_"+getWorkloadId(), "DETECT_FACE"+"_"+getWorkloadId(), 60, 0.1);
		addLink("FIND_MATCH"+"_"+getWorkloadId(), "DETECT_FACE"+"_"+getWorkloadId(), 60, 0.1);
		addLink("DETECT_FACE"+"_"+getWorkloadId(),"FACERECOGNIZER_OUTPUT"+"_"+getWorkloadId(),60, 0.1);
	}

	@Override
	public void sampleTasks() {
		double img_size = ExponentialDistributionGenerator.getNext(SimulationConstants.image_size); 
		addComponent("FACERECOGNIZER_UI"+"_"+getWorkloadId(), asList("python"),
				new Hardware(1, 1, 1)
				,false
				,ExponentialDistributionGenerator.getNext(1.0/2.0)
        		,5e+3
        		,img_size);
		addComponent("FIND_MATCH"+"_"+getWorkloadId(), asList("python"),
				new Hardware(4, 1, 1)
				,ExponentialDistributionGenerator.getNext(1.0/4.0)
        		,img_size
        		,img_size);
		addComponent("FIND_MATCH_INIT"+"_"+getWorkloadId(), asList("python"),
				new Hardware(4, 1, 1)
				,ExponentialDistributionGenerator.getNext(1.0/4.0)
        		,5e+3
        		,5e+3);
		addComponent("DETECT_FACE"+"_"+getWorkloadId(), asList("python"),
				new Hardware(4, 1, 1)
				,ExponentialDistributionGenerator.getNext(1.0/8.0)
        		,img_size
        		,img_size);
		addComponent("FACERECOGNIZER_OUTPUT"+"_"+getWorkloadId(), asList("python"),
				new Hardware(1, 1, 1)
				,false
				,ExponentialDistributionGenerator.getNext(1.0/4.0)
        		,img_size
        		,img_size);
		
	}

}
