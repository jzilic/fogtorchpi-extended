package at.ac.tuwien.ec.appmodel;

import java.util.ArrayList;
import java.util.List;

import di.unipi.socc.fogtorchpi.application.SoftwareComponent;
import di.unipi.socc.fogtorchpi.application.ThingRequirement;
import di.unipi.socc.fogtorchpi.utils.Hardware;

public class MobileSoftwareComponent extends SoftwareComponent {

	private boolean offloadable, visited;
	private double inData, outData, runtime = 0.0;
	private int rank;
	
	public MobileSoftwareComponent(String id) {
		super(id);
		// TODO Auto-generated constructor stub
	}

	public MobileSoftwareComponent(String identifier, List<String> softwareReqs, Hardware hardwareReqs, ArrayList<ThingRequirement> Theta){
		super(identifier,softwareReqs,hardwareReqs,Theta);
		inData = hardwareReqs.storage;
		outData = hardwareReqs.storage;
	}
	
	public void setOffloadable(boolean offloadable){
		this.offloadable = offloadable;
	}
	
	public boolean isOffloadable(){
		return offloadable;
	}

	public double getInData() {
		return inData;
	}

	public void setInData(double inData) {
		this.inData = inData;
	}

	public double getOutData() {
		return outData;
	}

	public void setOutData(double outData) {
		this.outData = outData;
	}
	
	public double getOffloadData(){
		return inData + getMips() * AVERAGE_INSTRUCTION_LENGTH;
		//return inData;
	}

	public double getRuntime() {
		return runtime;
	}

	public void setRuntime(double runtime) {
		this.runtime = runtime;
	}
	
	
}
