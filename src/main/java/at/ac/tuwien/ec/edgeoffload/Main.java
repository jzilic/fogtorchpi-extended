/**
 * Original file of paper revised according to FT refactoring with cost.
 */
package at.ac.tuwien.ec.edgeoffload;


import di.unipi.socc.fogtorchpi.application.Application;
import di.unipi.socc.fogtorchpi.application.ExactThing;
import di.unipi.socc.fogtorchpi.application.SoftwareComponent;
import di.unipi.socc.fogtorchpi.application.ThingRequirement;
import di.unipi.socc.fogtorchpi.deployment.Deployment;
import di.unipi.socc.fogtorchpi.deployment.MonteCarloSimulation;
import di.unipi.socc.fogtorchpi.infrastructure.Infrastructure;
import di.unipi.socc.fogtorchpi.utils.Couple;
import di.unipi.socc.fogtorchpi.utils.Hardware;
import di.unipi.socc.fogtorchpi.utils.QoS;
import di.unipi.socc.fogtorchpi.utils.QoSProfile;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import static java.util.Arrays.asList;
import java.util.HashMap;
import java.util.Scanner;

import at.ac.tuwien.ec.appmodel.MobileApplication;
import at.ac.tuwien.ec.infrastructuremodel.MobileCloudInfrastructure;
import at.ac.tuwien.ec.utils.DeploymentsHistogram;
import at.ac.tuwien.ec.utils.ExponentialDistributionGenerator;
import at.ac.tuwien.ec.utils.MontecarloStatisticsPrinter;

/**
 *
 * @author Stefano
 */
public class Main {

    
	
    
	public static void main(String[] args) {
		processArgs(args);

		MobileCloudInfrastructure I = new MobileCloudInfrastructure() ;

		Helper.setupInfrastructure(I);



		DateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd-HH_mm_ss");
		Date date = new Date();

		HashMap<String,DeploymentsHistogram> histograms;
		//System.out.println(I);
		PrintWriter[] writers = new PrintWriter[SimulationConstants.algorithms.length];
		for(int i = 0; i < SimulationConstants.algorithms.length; i++){
			String filename = "./results/"
					+ SimulationConstants.algorithms[i] +"/"
					+ dateFormat.format(date) 
					+ "-" + SimulationConstants.targetApp
					+ "-CLOUD=" + SimulationConstants.cloudNodes
					+ "-EDGE=" + SimulationConstants.edgeNodes
					+ "-" + selectAppArguments(SimulationConstants.targetApp)
					+ "-" + SimulationConstants.algorithms[i]
							+ ((SimulationConstants.algorithms[i].equals("weighted"))? 
									"-alpha="+SimulationConstants.alpha+"-beta="+SimulationConstants.beta+"-gamma="+SimulationConstants.gamma
									: "") 
							+ ((SimulationConstants.cloudOnly)? "-ONLYCLOUD":
								"-eta-" + SimulationConstants.Eta)
							+ ".data";
			try {
				writers[i] = new PrintWriter(filename,"UTF-8");
				writers[i].println(MontecarloStatisticsPrinter.getHeader());
			} 
			catch (FileNotFoundException | UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		//for(int i = 0; i < SimulationConstants.appExecutions.length; i++)
		int[] offloadableParts = {25,50,75,100};
		for(int i = 0; i < offloadableParts.length; i++)
		{
			//ArrayList<MobileApplication> workload = Helper.setupWorkload(SimulationConstants.appExecutions[i]);
			ArrayList<MobileApplication> workload = Helper.setupWorkload(1);
			MonteCarloSimulation s = new MonteCarloSimulation(SimulationConstants.simulation_runs, workload, I);
			long startTime = System.nanoTime();
			histograms = s.startSimulation(asList());
			long endTime = System.nanoTime();
			SimulationConstants.offloadable_part_repetitions = offloadableParts[i];
			for(int j = 0; j < SimulationConstants.algorithms.length; j++)
			{
				try {
					if(!SimulationConstants.batch)
						System.out.println(MontecarloStatisticsPrinter.getHeader());
					int k = 0;
					for (Deployment dep : histograms.get(SimulationConstants.algorithms[j]).keySet()) {
						writers[j].print(MontecarloStatisticsPrinter.getStatistics(offloadableParts[i], dep, histograms.get(SimulationConstants.algorithms[j]), SimulationConstants.simulation_runs));
						if(!SimulationConstants.batch)
							System.out.print(k + " - " + MontecarloStatisticsPrinter.getStatistics(offloadableParts[i], dep, histograms.get(SimulationConstants.algorithms[j]), SimulationConstants.simulation_runs));
						k++;
					}

					//writer.println("Runtime: " + (endTime - startTime)/1e+9 + " seconds.");
					//writer.println("ETA: " + SimulationConstants.Eta);

				} catch (Exception e) {
					e.printStackTrace();
				}
				j++;
			}
		}
		for(PrintWriter p: writers) p.close();
	}


	private static String selectAppArguments(String targetApp) {
		String tmp = "";
		switch(targetApp){
		case "NAVI": 
			tmp+="maps_size="+SimulationConstants.maps_size;
			break;
		case "ANTIVIRUS":
			tmp+="file_size="+SimulationConstants.file_size;
			break;
		case "FACEREC":
			tmp+="image_size="+SimulationConstants.image_size;
			break;
		case "CHESS":
			tmp+="chess_mi="+SimulationConstants.chess_mi;
			break;
		case "FACEBOOK":
			tmp+="image_size="+SimulationConstants.image_size;
			break;
		}
		return tmp;
	}


	private static void processArgs(String[] args) {
		for(String s : args)
		{
			if(s.startsWith("-battery="))
			{
				String[] tmp = s.split("=");
				SimulationConstants.batteryBudget = Double.parseDouble(tmp[1]);
				continue;
			}
			if(s.startsWith("-cloud="))
			{
				String[] tmp = s.split("=");
				SimulationConstants.cloudNodes = Integer.parseInt(tmp[1]);
				continue;
			}
			if(s.startsWith("-edge="))
			{
				String[] tmp = s.split("=");
				SimulationConstants.edgeNodes = Integer.parseInt(tmp[1]);
				continue;
			}
			if(s.startsWith("-wl-runs=")){
				String[] tmp = s.split("=");
				SimulationConstants.appExecutions[0] = Integer.parseInt(tmp[1]);
				continue;
			}
			if(s.equals("-batch"))
				SimulationConstants.batch = true;
			if(s.startsWith("-map-size="))
			{
				String[] tmp = s.split("=");
				SimulationConstants.maps_size = 1.0 / Double.parseDouble(tmp[1]);
				continue;
			}
			if(s.startsWith("-file-size="))
			{
				String[] tmp = s.split("=");
				// 1/input, to be used for lambda of exponential distribution
				SimulationConstants.file_size = 1.0 / Double.parseDouble(tmp[1]);
				continue;
			}
			if(s.startsWith("-image-size="))
			{
				String[] tmp = s.split("=");
				SimulationConstants.image_size = 1.0 / Double.parseDouble(tmp[1]);
				continue;
			}
			if(s.startsWith("-chess-mi="))
			{
				String[] tmp = s.split("=");
				SimulationConstants.chess_mi = (1.0/Double.parseDouble(tmp[1]));
				continue;
			}
			if(s.startsWith("-alpha="))
			{
				String[] tmp = s.split("=");
				SimulationConstants.alpha = Double.parseDouble(tmp[1]);
			}
			if(s.startsWith("-beta="))
			{
				String[] tmp = s.split("=");
				SimulationConstants.beta = Double.parseDouble(tmp[1]);
			}
			if(s.startsWith("-gamma="))
			{
				String[] tmp = s.split("=");
				SimulationConstants.gamma = Double.parseDouble(tmp[1]);
			}
			if(s.startsWith("-eta="))
			{
				String[] tmp = s.split("=");
				SimulationConstants.Eta = Double.parseDouble(tmp[1]);
				continue;
			}
			if(s.startsWith("-app="))
			{
				String[] tmp = s.split("=");
				SimulationConstants.targetApp = tmp[1];
				continue;
			}
			if(s.startsWith("-algo="))
			{
				String[] tmp = s.split("=");
				SimulationConstants.algorithms[0] = tmp[1];
				continue;
			}
			if(s.equals("-cloudonly"))
				SimulationConstants.cloudOnly = true;
		}
	}


	private static Deployment getLowestScoreDeployment(DeploymentsHistogram histogram, double minRuntime,
			double maxBattery, double minUserCost) {
		double minScore = Double.MAX_VALUE;
		double alpha = 1.0, beta = 1.0, gamma = 1.0;
		Deployment target = null;
		
		for(Deployment d: histogram.keySet()){
			double currRuntime = d.runTime;
			double currCost = d.deploymentMonthlyCost.getCost();
			double currBattery = d.mobileEnergyBudget;
		
			double tmpScore =  alpha * (currRuntime - minRuntime) + beta * (currCost - minUserCost) + gamma * (maxBattery - currBattery);
			if(tmpScore < minScore)
				target = d;
		}
		return target;
	}
}
