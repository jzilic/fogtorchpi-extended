package at.ac.tuwien.ec.edgeoffload;

import static java.util.Arrays.asList;

import java.util.ArrayList;
import java.util.Random;

import at.ac.tuwien.ec.appmodel.MobileApplication;
import at.ac.tuwien.ec.appmodel.mobileapps.AntivirusApp;
import at.ac.tuwien.ec.appmodel.mobileapps.ChessApp;
import at.ac.tuwien.ec.appmodel.mobileapps.FaceRecognizerApp;
import at.ac.tuwien.ec.appmodel.mobileapps.FacebookApp;
import at.ac.tuwien.ec.appmodel.mobileapps.NavigatorApp;
import at.ac.tuwien.ec.appmodel.mobileapps.NavigatorAppExp;
import at.ac.tuwien.ec.infrastructuremodel.MobileCloudInfrastructure;
import at.ac.tuwien.ec.infrastructuremodel.MobileDevice;
import at.ac.tuwien.ec.infrastructuremodel.energy.AMDCPUEnergyModel;
import at.ac.tuwien.ec.infrastructuremodel.energy.Mobile3GNETEnergyModel;
import at.ac.tuwien.ec.infrastructuremodel.energy.MobileWiFiNETEnergyModel;
import at.ac.tuwien.ec.infrastructuremodel.energy.SamsungS2DualEnergyModel;
import at.ac.tuwien.ec.utils.ExponentialDistributionGenerator;
import di.unipi.socc.fogtorchpi.application.ExactThing;
import di.unipi.socc.fogtorchpi.application.ThingRequirement;
import di.unipi.socc.fogtorchpi.infrastructure.Infrastructure;
import di.unipi.socc.fogtorchpi.utils.Couple;
import di.unipi.socc.fogtorchpi.utils.Hardware;
import di.unipi.socc.fogtorchpi.utils.QoS;
import di.unipi.socc.fogtorchpi.utils.QoSProfile;



public class Helper {
	
	public static ArrayList<MobileApplication> setupWorkload(int appExecutions){
		ArrayList<MobileApplication> workload = new ArrayList<MobileApplication>();
		
		/*switch(SimulationConstants.targetApp){
		case "NAVI":
			workload.add(new NavigatorAppExp(0));
			break;
		case "CHESS":
			workload.add(new ChessApp(0));
			break;
		case "ANTIVIRUS":
			workload.add(new AntivirusApp(0));
			break;
		case "FACEREC":
			workload.add(new FaceRecognizerApp());
			break;
		}
		/*FaceRecognizerApp fr = new FaceRecognizerApp();
		ChessApp ca = new ChessApp();
		ca.sample();
		fr.sample();*/
		
		//workload.add(new FacebookApp(0));
		for(int i = 0; i < appExecutions; i++)
		{
			switch(SimulationConstants.targetApp){
			case "NAVI":
				workload.add(new NavigatorAppExp(i));
				break;
			case "CHESS":
				workload.add(new ChessApp(i));
				break;
			case "ANTIVIRUS":
				workload.add(new AntivirusApp(i));
				break;
			case "FACEREC":
				workload.add(new FaceRecognizerApp(i));
				break;
			case "FACEBOOK":
				workload.add(new FacebookApp(i));
				break;
			}
		}
		//workload.add(new NavigatorAppExp(2));
		//workload.add(new ChessApp(3));
		//workload.add(new AntivirusApp(4));
		return workload;
    }
	
	public static void setupInfrastructure(MobileCloudInfrastructure I){
                 
        int cloudNodes = (SimulationConstants.cloudOnly)? 
        		SimulationConstants.cloudNodes + SimulationConstants.edgeNodes : 
        		SimulationConstants.cloudNodes;
        
        for(int i = 0; i < cloudNodes; i++)
        	I.addCloudDatacentre("cloud_"+i, asList(
        		new Couple<String,Double>("spark",0.0),
        		new Couple<String,Double>("mySQL",0.0),
        		new Couple<String,Double>("linux",0.0),
        		new Couple<String,Double>("windows",0.0),
        		new Couple<String,Double>("python",0.0),
        		new Couple<String,Double>("c++",0.0),
        		new Couple<String,Double>(".NETcore",0.0)),
        		52.195097, 3.0364791,
        		new Hardware(64, 128.0, (int)1e12, 0.03, 0.02, 0.01),
        		new AMDCPUEnergyModel(),
        		15);
        
        //Fog nodes
        if(!SimulationConstants.cloudOnly)
           	for(int i = 0; i < SimulationConstants.edgeNodes; i++)
        		I.addFogNode("edge_"+i, asList(new Couple<String,Double>("python",0.0),
        				new Couple<String,Double>("c++",0.0),
        				new Couple<String,Double>("mySQL",0.0),
        				new Couple<String,Double>(".NETcore",0.0),
        				new Couple<String,Double>("linux",0.0)),
        				//new Hardware((int) Math.floor(SimulationConstants.rand.nextGaussian() * SimulationConstants.core_variance + 8), 128, 1000, 0.03, 0.02, 0.01), 43.740186, 10.364619,
        				new Hardware(16, 128, (int)1e12, 0.03, 0.02, 0.01), 43.740186, 10.364619,
        				//new Hardware((int) Math.floor(ExponentialDistributionGenerator.getNext(1.0/16.0)), 128, 1000, 0.03, 0.02, 0.01), 43.740186, 10.364619,
        				new AMDCPUEnergyModel(),
        				15);
        
        //Mobile nodes
        MobileDevice device;
        device = new MobileDevice("mobile",
        		asList(new Couple<String,Double>("python",0.0)),
        		new Hardware(2,8,(int)16e10),
        		43.78,10.45);
        device.setCPUEnergyModel(new SamsungS2DualEnergyModel());
        
        device.setMipsPerCore(4);
        device.setEnergyBudget(SimulationConstants.batteryBudget);
        I.setMobileDevice(device);
        //Qos Profiles
        QoSProfile qos3gUP = new QoSProfile(asList(
                new Couple<QoS,Double>(new QoS(54.0, 7.2), 0.9957),
                new Couple<QoS,Double>(new QoS(Double.MAX_VALUE, 0.0), 0.0043)));
        QoSProfile qos3gDN = new QoSProfile(asList(
                new Couple<QoS,Double>(new QoS(54.0, 7.2), 0.9959),
                new Couple<QoS,Double>(new QoS(Double.MAX_VALUE, 0.0), 0.0041)));
        QoSProfile qosWifiUP = new QoSProfile(asList(
                new Couple<QoS,Double>(new QoS(15.0, 32.0), 0.9),
                new Couple<QoS,Double>(new QoS(15.0, 4.0), 0.09),
                new Couple<QoS,Double>(new QoS(Double.MAX_VALUE, 0), 0.01)
        ));
        QoSProfile qosWifiDN = new QoSProfile(asList(
                new Couple<QoS,Double>(new QoS(15.0, 32.0), 0.9),
                new Couple<QoS,Double>(new QoS(15.0, 4.0), 0.09),
                new Couple<QoS,Double>(new QoS(Double.MAX_VALUE, 0), 0.01)
        ));
        
        int cloudLatency = (int) (SimulationConstants.rand.nextGaussian() * 33.5 + 200);
        QoSProfile qos3gUPCloud = new QoSProfile(asList(
                new Couple<QoS,Double>(new QoS(54.0 + cloudLatency, 7.2), 0.9957),
                new Couple<QoS,Double>(new QoS(Double.MAX_VALUE, 0.0), 0.0043)));
        QoSProfile qos3gDNCloud = new QoSProfile(asList(
                new Couple<QoS,Double>(new QoS(54.0 + cloudLatency, 7.2), 0.9959),
                new Couple<QoS,Double>(new QoS(Double.MAX_VALUE, 0.0), 0.0041)));
        QoSProfile qosWifiUPCloud = new QoSProfile(asList(
                new Couple<QoS,Double>(new QoS(15.0 + cloudLatency, 32), 0.9),
                new Couple<QoS,Double>(new QoS(15.0 + cloudLatency, 4), 0.09),
                new Couple<QoS,Double>(new QoS(Double.MAX_VALUE, 0), 0.01)));
        QoSProfile qosWifiDNCloud = new QoSProfile(asList(
                new Couple<QoS,Double>(new QoS(15.0 + cloudLatency, 32), 0.9),
                new Couple<QoS,Double>(new QoS(15.0 + cloudLatency, 4), 0.09),
                new Couple<QoS,Double>(new QoS(Double.MAX_VALUE, 0), 0.01)
        ));
        QoSProfile qosUpFog,qosDownFog,qosDownCloud,qosUpCloud;
        boolean wifi = SimulationConstants.rand.nextDouble() < 0.25;
        device.setNetEnergyModel((wifi)? new MobileWiFiNETEnergyModel() : new Mobile3GNETEnergyModel());
        qosUpFog = (wifi)? qosWifiUP : qos3gUP;
        qosDownFog = (wifi)? qosWifiDN : qos3gDN;
        qosUpCloud = (wifi)? qosWifiUPCloud : qos3gUPCloud;
        qosDownCloud = (wifi)? qosWifiDNCloud : qos3gDNCloud;
                
        //Links
        if(!SimulationConstants.cloudOnly)
        	for(int i = 0; i < SimulationConstants.edgeNodes; i++)
        	{
        		I.addLink("mobile", "edge_" + i,qosUpFog,qosDownFog);
        		I.addLink("edge_" + i, "mobile",qosUpFog,qosDownFog);
        	}
        for(int i = 0; i < cloudNodes; i++)
        {
        	I.addLink("mobile", "cloud_"+i,qosUpCloud,qosDownCloud);
        	I.addLink("cloud_"+i, "mobile",qosUpCloud,qosDownCloud);
        }
        	
        //System.out.println("Setup infrastructure with " + I.C.size() + " Cloud nodes and " + I.F.size() + " Edge nodes.");
       
    }
}
