package at.ac.tuwien.ec.edgeoffload;

import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SimulationConstants {

	/*
	 * SIMULATION RELATED CONSTANTS	
	 */
	public static boolean batch = false;
	public static int offloadable_part_repetitions = 1;
	public static int[] appExecutions = {5,10,15,20};
	public static double Eta = 0.01;
	public static String targetApp = "NAVI";
	public static final int simulation_runs = 100000;
	//public static String chosenAlgorithm = "weighted";
	public static Level logLevel = Level.OFF;
	public static double workload_runs_lambda = 1.0;
	//PROBABILITY DISTRIBUTIONS
	public static final Random rand= new Random();
	//DEBUGGING
	public static final Logger logger = Logger.getLogger("logger");
	
	/* 
	 * APP RELATED CONSTANTS
	 */
	//ANTIVIRUS
	public static final int antivirusSmallFiles = 1;
	public static final int antivirusAverageFiles = 1;
	public static final int antivirusBigFiles = 1;
	//lambda
	public static double file_size = 10e-6;
	//FACERECOGNIZER
	public static double image_size = 10e-3; //lambda
	//NAVIGATOR
	public static double maps_size = 25e-6; //lambda
	//CHESS
	public static final int chessMoves = 1;
	public static double chess_mi = 8e-1; //lambda
	
	/*
	 *  INFRASTRUCTURE RELATED CONSTANTS
	 */
	public static boolean cloudOnly = false;
	public static double alpha = 1.0, beta = 1.0, gamma = 1.0;
	public static double core_variance = 2.6;
	public static int cloudNodes = 1;
	public static int edgeNodes = 1;
	public static final int cloudNodesOnlyCloud = cloudNodes + edgeNodes;
	public static final double confidenceLevel = 2.576;
	//public static final String[] algorithms = {"weighted", "mincost", "maxbatt","minmin"};
	public static final String[] algorithms = {"weighted"};
	public static double batteryBudget = 10.0;
	
	
}
