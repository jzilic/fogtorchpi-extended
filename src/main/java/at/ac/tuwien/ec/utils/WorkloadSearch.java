package at.ac.tuwien.ec.utils;
import java.util.ArrayList;
import java.util.HashMap;

import at.ac.tuwien.ec.algorithms.BacktrackingExhaustiveResearch;
import at.ac.tuwien.ec.algorithms.FirstFitResearch;
import at.ac.tuwien.ec.algorithms.HEFTResearch;
import at.ac.tuwien.ec.algorithms.MAXBattery;
import at.ac.tuwien.ec.algorithms.MINCostResearch;
import at.ac.tuwien.ec.algorithms.MinMinResearch;
import at.ac.tuwien.ec.algorithms.WeightedFunctionResearch;
import at.ac.tuwien.ec.appmodel.*;
import at.ac.tuwien.ec.edgeoffload.Helper;
import at.ac.tuwien.ec.edgeoffload.SimulationConstants;
import at.ac.tuwien.ec.infrastructuremodel.MobileCloudInfrastructure;
import di.unipi.socc.fogtorchpi.deployment.Deployment;
import di.unipi.socc.fogtorchpi.deployment.Search;

public class WorkloadSearch{
	
	private ArrayList<MobileApplication> workload;
	private String chosenAlgorithm;
	
	public WorkloadSearch(ArrayList<MobileApplication> workload){
		this.workload = workload;
	}
	
	public WorkloadSearch(ArrayList<MobileApplication> workload, String chosenAlgorithm){
		this.workload = workload;
		this.chosenAlgorithm = chosenAlgorithm;
	}
		
	public ArrayList<Deployment> findDeployments(MobileCloudInfrastructure I){
		MobileApplication currentApp;
		Search singleSearch = null;
		ArrayList<Deployment> deployments = new ArrayList<Deployment>();
			
		Deployment dep = new Deployment();
		
		for(int i = 0; i < workload.size(); i++)
		{
			currentApp = workload.get(i);
			SimulationConstants.logger.info("Application: " + currentApp.getClass().getSimpleName() + ", " + i + " of " + workload.size());
			SimulationConstants.logger.warning(I.toString());
			switch(SimulationConstants.algorithms[0]){
				case "exhaustive":
					singleSearch = new BacktrackingExhaustiveResearch(currentApp, I);
					break;
				case "weighted":
					singleSearch = new WeightedFunctionResearch(currentApp, I);
					break;
				case "firstfit":
					singleSearch = new FirstFitResearch(currentApp, I, null);
					break;
				case "heft":
					SimulationConstants.logger.info("Starting HEFT algorithm");
					singleSearch = new HEFTResearch(currentApp, I);
					break;
				case "mincost":
					singleSearch = new MINCostResearch(currentApp, I);
					break;
				case "maxbatt":
					singleSearch = new MAXBattery(currentApp, I);
					break;
				default:
					singleSearch = new FirstFitResearch(currentApp, I, null);
			}
			singleSearch.findDeployments(dep);
			if(dep != null && !outliersDetected(dep))
			{
				deployments.add((Deployment)dep.clone());
				singleSearch.resetSearch(dep);
			}
		}
		
		return deployments;
	}

	public ArrayList<Deployment> findDeployments(MobileCloudInfrastructure I, MobileApplication app, String algorithm){
		MobileApplication currentApp;
		Search singleSearch = null;
		ArrayList<Deployment> deployments = new ArrayList<Deployment>();

		Deployment dep = new Deployment();

		currentApp = app;
		SimulationConstants.logger.info("Application: " + currentApp.getClass().getSimpleName());
		SimulationConstants.logger.warning(I.toString());
		switch(algorithm){
		case "exhaustive":
			singleSearch = new BacktrackingExhaustiveResearch(currentApp, I);
			break;
		case "weighted":
			singleSearch = new WeightedFunctionResearch(currentApp, I);
			break;
		case "firstfit":
			singleSearch = new FirstFitResearch(currentApp, I, null);
			break;
		case "heft":
			SimulationConstants.logger.info("Starting HEFT algorithm");
			singleSearch = new HEFTResearch(currentApp, I);
			break;
		case "mincost":
			singleSearch = new MINCostResearch(currentApp, I);
			break;
		case "maxbatt":
			singleSearch = new MAXBattery(currentApp, I);
			break;
		case "minmin":
			singleSearch = new MinMinResearch(currentApp, I);
			break;
		default:
			singleSearch = new FirstFitResearch(currentApp, I, null);
		}
		singleSearch.findDeployments(dep);
		if(dep != null && !outliersDetected(dep))
		{
			deployments.add((Deployment)dep.clone());
			//singleSearch.resetSearch(dep);
		}

		return deployments;
	}
	
	private boolean outliersDetected(Deployment dep) {
		if(dep.mobileEnergyBudget <= 0.0
				|| dep.runTime <= 0.001
				|| dep.deploymentMonthlyCost.getCost() <= 0.0)
			return true;
		return false;
	}
	
	
}

