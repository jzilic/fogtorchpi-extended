package at.ac.tuwien.ec.algorithms;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;

import at.ac.tuwien.ec.appmodel.MobileApplication;
import at.ac.tuwien.ec.appmodel.MobileSoftwareComponent;
import at.ac.tuwien.ec.infrastructuremodel.MobileCloudInfrastructure;
import di.unipi.socc.fogtorchpi.application.SoftwareComponent;
import di.unipi.socc.fogtorchpi.deployment.Deployment;
import di.unipi.socc.fogtorchpi.deployment.Search;
import di.unipi.socc.fogtorchpi.infrastructure.ComputationalNode;

public class BacktrackingExhaustiveResearch extends Search{

	public BacktrackingExhaustiveResearch(MobileApplication A, MobileCloudInfrastructure I) {
		super(A, I);
		// TODO Auto-generated constructor stub
	}

	public BacktrackingExhaustiveResearch(MobileApplication A, MobileCloudInfrastructure I, HashMap<String, HashSet<String>> businessPolicies){
		super(A,I,businessPolicies);
	}
	
	public Deployment searchOnDAG(Deployment deployment) {
		if (isComplete(deployment)) {
            D.add((Deployment) deployment.clone());
            System.out.println(deployment);
            return deployment;
        }
        //SoftwareComponent s = selectUndeployedComponent(deployment);
        //System.out.println("Selected :" + s);
		ArrayList<SoftwareComponent> eligibleNodes = selectElegibleNodes();
		HashMap<ComputationalNode,MobileSoftwareComponent> partialDeploy = new HashMap<ComputationalNode,MobileSoftwareComponent>();
		if(eligibleNodes.isEmpty())
			return null;
		SoftwareComponent s = eligibleNodes.remove(0);
		ArrayList<ComputationalNode> Ks = bestFirst(K.get(s.getId()),s);
        //System.out.println(K.get(s.getId()));
        for (ComputationalNode n : Ks) { // for all nodes compatible with s 
            steps++;
            //System.out.println(steps + " Checking " + s.getId() + " onto " + n.getId());
            if (isValid(deployment, s, n)) {
                //System.out.println(steps + " Deploying " + s.getId() + " onto " + n.getId());
                deploy(deployment, s, n);
                deployment.addRuntime(s, n, I);
                if(searchOnDAG(deployment) == null){
                	undeploy(deployment, s, n);
                	deployment.removeRuntime(s,n,I);
                }
            }
            //System.out.println(steps + " Undeploying " + s.getId() + " from " + n.getId());
        }
        return deployment;
	}
	
	@Override
	public Deployment search(Deployment deployment) {
		if (isComplete(deployment)) {
            D.add((Deployment) deployment.clone());
            //System.out.println(deployment);
            return deployment;
        }
        SoftwareComponent s = selectUndeployedComponent(deployment);
        //System.out.println("Selected :" + s);
        ArrayList<ComputationalNode> Ks = bestFirst(K.get(s.getId()),s);
        //System.out.println(K.get(s.getId()));
        for (ComputationalNode n : Ks) { // for all nodes compatible with s 
            steps++;
            //System.out.println(steps + " Checking " + s.getId() + " onto " + n.getId());
            if (isValid(deployment, s, n)) {
                //System.out.println(steps + " Deploying " + s.getId() + " onto " + n.getId());
                deploy(deployment, s, n);
                deployment.addRuntime(s, n, I);
                search(deployment);
                undeploy(deployment, s, n);
                deployment.removeRuntime(s,n,I);
            }
            //System.out.println(steps + " Undeploying " + s.getId() + " from " + n.getId());
        }
        return null;
	}

	@Override
	public void findDeployments(Deployment deployment) {
		
        
        //System.out.println(A.S);
        findCompatibleNodes();
        	
        Collections.sort(A.S, (Object o1, Object o2) -> {
            SoftwareComponent s1 = (SoftwareComponent) o1;
            SoftwareComponent s2 = (SoftwareComponent) o2;
            return Integer.compare(K.get(s1.getId()).size(), K.get(s2.getId()).size());             
        });
        
        searchOnDAG(deployment);
        
     }
	
	

}
