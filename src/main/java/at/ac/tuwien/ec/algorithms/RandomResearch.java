package at.ac.tuwien.ec.algorithms;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Random;

import at.ac.tuwien.ec.appmodel.MobileApplication;
import at.ac.tuwien.ec.appmodel.MobileSoftwareComponent;
import at.ac.tuwien.ec.infrastructuremodel.MobileCloudInfrastructure;
import di.unipi.socc.fogtorchpi.application.SoftwareComponent;
import di.unipi.socc.fogtorchpi.deployment.Deployment;
import di.unipi.socc.fogtorchpi.deployment.Search;
import di.unipi.socc.fogtorchpi.infrastructure.ComputationalNode;

public class RandomResearch extends Search {
	final static int maxTries = 100;
	
	public RandomResearch(MobileApplication A, MobileCloudInfrastructure I,
			HashMap<String, HashSet<String>> businessPolicies) {
		super(A, I, businessPolicies);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void findDeployments() {
		Deployment deployment = new Deployment();
        
        //System.out.println(A.S);
        findCompatibleNodes();
        	
        Collections.sort(A.S, (Object o1, Object o2) -> {
            SoftwareComponent s1 = (SoftwareComponent) o1;
            SoftwareComponent s2 = (SoftwareComponent) o2;
            return Integer.compare(K.get(s1.getId()).size(), K.get(s2.getId()).size());             
        });
        
        search(deployment);
        resetSearch(deployment);
	}
	
	public Deployment search(Deployment deployment) {
		while(!isComplete(deployment)){
			SoftwareComponent s = selectUndeployedComponent(deployment);
	        MobileSoftwareComponent msc = (MobileSoftwareComponent) s;
	        if(!msc.isOffloadable())
	           	deploy(deployment,s,I.getMobileDevice());
	        else
	        {
	        	if (K.get(s.getId()) != null) 
	        	{
	                ArrayList<ComputationalNode> compNodes = K.get(s.getId());
	                Random r = new Random();
	                int index = r.nextInt(compNodes.size());
	                int numTries = 0;
	                while(!isValid(deployment,s,compNodes.get(index))
	                		&& numTries < maxTries) {
	                	index = r.nextInt(compNodes.size());
	                	numTries++;
	                }
	                if(numTries == maxTries){
	                	resetSearch(deployment);
	                	return null;
	                }
	                deploy(deployment,s,compNodes.get(index));
	                numTries = 0;
	            }
	            else{
	            	resetSearch(deployment);
	            	return null; 
	            }
	         }
		}
		D.add((Deployment) deployment.clone());
		return deployment;
	}
}
