package at.ac.tuwien.ec.algorithms;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.Stack;
import java.util.logging.Level;
import java.util.logging.Logger;

import at.ac.tuwien.ec.appmodel.MobileApplication;
import at.ac.tuwien.ec.appmodel.MobileSoftwareComponent;
import at.ac.tuwien.ec.edgeoffload.SimulationConstants;
import at.ac.tuwien.ec.infrastructuremodel.MobileCloudInfrastructure;
import di.unipi.socc.fogtorchpi.application.SoftwareComponent;
import di.unipi.socc.fogtorchpi.deployment.Deployment;
import di.unipi.socc.fogtorchpi.deployment.Search;
import di.unipi.socc.fogtorchpi.infrastructure.ComputationalNode;
import di.unipi.socc.fogtorchpi.utils.Couple;

public class HEFTResearch extends DAGResearch {
	
	class RankComparator implements Comparator<MobileSoftwareComponent>{

		@Override
		public int compare(MobileSoftwareComponent o1, MobileSoftwareComponent o2) {
			// TODO Auto-generated method stub
			return 0;
		}

		
	}
			
	public HEFTResearch(MobileApplication A, MobileCloudInfrastructure I) {
		super(A, I);
	}
	
	public HEFTResearch(MobileApplication A, MobileCloudInfrastructure I,
			HashMap<String, HashSet<String>> businessPolicies) {
		super(A, I, businessPolicies);
		// TODO Auto-generated constructor stub
	}

	public Deployment searchOnDAG(Deployment deployment) {
		ArrayList<SoftwareComponent> componentLists = A.S;
		componentLists.sort(c);
		return deployment;
	}
	
	protected ComputationalNode findTarget(Deployment deployment, MobileSoftwareComponent msc) {
		ComputationalNode target = null;
		if(!msc.isOffloadable())
		{
			if(isValid(deployment,msc,I.getMobileDevice()))
				return I.getMobileDevice();
			else
				return null;
		}
		else
		{
			ArrayList<ComputationalNode> compNodes = compatibleNodes(deployment, msc);
			double currRuntime = Double.MAX_VALUE;
			for(int i = 0; i < compNodes.size(); i++)
			{
				ComputationalNode cn = compNodes.get(i);
				if(!isValid(deployment,msc,cn))
					continue;
				else
				{
					SimulationConstants.logger.info("currRuntime: "
							+currRuntime + " runtime on node " + cn.getId() + "=" + msc.getRuntimeOnNode(cn, I));
					if(msc.getRuntimeOnNode(cn, I) < currRuntime)
					{
						target = cn;
						currRuntime = msc.getRuntimeOnNode(cn, I);
					}
				}
			}
		}
		return target;						

	}
	
}
