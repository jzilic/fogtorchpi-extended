/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.ac.tuwien.ec.infrastructuremodel;

import di.unipi.socc.fogtorchpi.application.SoftwareComponent;
import di.unipi.socc.fogtorchpi.deployment.Deployment;
import di.unipi.socc.fogtorchpi.infrastructure.CloudDatacentre;
import di.unipi.socc.fogtorchpi.infrastructure.ComputationalNode;
import di.unipi.socc.fogtorchpi.infrastructure.FogNode;
import di.unipi.socc.fogtorchpi.infrastructure.Infrastructure;
import di.unipi.socc.fogtorchpi.utils.Couple;
import di.unipi.socc.fogtorchpi.utils.Hardware;
import di.unipi.socc.fogtorchpi.utils.QoSProfile;

import static java.util.Arrays.asList;

import java.util.List;

import at.ac.tuwien.ec.appmodel.MobileSoftwareComponent;
import at.ac.tuwien.ec.edgeoffload.Helper;
import at.ac.tuwien.ec.edgeoffload.SimulationConstants;
import at.ac.tuwien.ec.infrastructuremodel.energy.AMDCPUEnergyModel;
import at.ac.tuwien.ec.infrastructuremodel.energy.CPUEnergyModel;
import at.ac.tuwien.ec.utils.ExponentialDistributionGenerator;

/**
 *
 * @author Vincenzo
 */
public class MobileCloudInfrastructure extends Infrastructure {
    
	private MobileDevice mobileDevice;
	
	public MobileCloudInfrastructure()
	{
		super();
	}
	
	public String getVMList(){
		String s = "";
		for(CloudDatacentre c: C.values())
			s += c.getVirtualMachines();
		
		return s;
	}
	
	public void setMobileDevice(MobileDevice device)
	{
		this.mobileDevice = device;
		L.put(new Couple(device.getId(),device.getId()), new QoSProfile(0, Double.MAX_VALUE));
	}
	
	public MobileDevice getMobileDevice(){
		return mobileDevice;
	}
	
	public Couple<Double, Double> consumedResources(Deployment d){
        
        Couple<Double, Double> consumedResources = new Couple(0.0,0.0);
        Couple<Double, Double> allResources = new Couple(0.0,0.0);
        
        for (FogNode g : F.values()){
                allResources.setA(g.getHardware().ram + allResources.getA());
                allResources.setB(g.getHardware().storage + allResources.getB());
                
        }
        
        for (SoftwareComponent s: d.keySet()){
            if (F.containsKey(d.get(s).getId())){
            	
                FogNode f = (FogNode) d.get(s); 
                Couple<Double, Double> tmp = f.consumedResources(s);
                
                consumedResources.setA(tmp.getA() + consumedResources.getA());
                consumedResources.setB(tmp.getB() + consumedResources.getB());
            }
        }
        
        return new Couple<Double,Double>(consumedResources.getA()/allResources.getA(),
                consumedResources.getB()/allResources.getB());
    }

	public void addFogNode(String identifier, List<Couple<String, Double>> software, Hardware h, double x, double y,
			List<Couple<String, Double>> vmTypes) {
		F.put(identifier,new FogNode(identifier, software, x, y, h, vmTypes));
        L.put(new Couple(identifier,identifier), new QoSProfile(0, Double.MAX_VALUE));
		
	}

	public void addCloudDatacentre(String string, List<Couple<String, Double>> asList, double d, double e,
			Hardware hardware, CPUEnergyModel cpuEnergyModel, double i) {
		C.put(string,new CloudDatacentre(string,asList,d,e,hardware,null,cpuEnergyModel,i));
		L.put(new Couple(string,string), new QoSProfile(0, Double.MAX_VALUE));
	}
	
	public void addFogNode(String identifier, List<Couple<String, Double>> software, Hardware h, double x, double y,
			CPUEnergyModel cpuEnergyModel, double i) {
		F.put(identifier,new FogNode(identifier, software, x, y, h,null, cpuEnergyModel, i));
        L.put(new Couple(identifier,identifier), new QoSProfile(0, Double.MAX_VALUE));
		
	}

	public double getTransmissionTime(MobileSoftwareComponent s,String id1, String id2) {
		QoSProfile profile = L.get(new Couple<String,String>(id1,id2));
		if(profile == null)
			profile = L.get(new Couple<String,String>(id2,id1));
		if(profile.getLatency()==Integer.MAX_VALUE)
			return Double.MAX_VALUE;
		//return s.getOffloadData()/(profile.getBandwidth()*8e+6) + 
		//		((s.getOffloadData()/65535)*(profile.getLatency()/1000));
		return (((s.getOutData() + s.getOffloadData())/(profile.getBandwidth()*125000.0) + 
				(profile.getLatency()/1000.0)) )* SimulationConstants.offloadable_part_repetitions;
	}
	
	public boolean isCloudNode(String id){
		return C.containsKey(id);
	}
	
	public boolean isFogNode(String id){
		return F.containsKey(id);
	}

	public void sample() {
		for(QoSProfile q: L.values())
			q.sampleQoS();
	}

	public String toString(){
		String tmp = "";
		tmp += "CLOUD NODES:\n";
		tmp += C + " ;\n";
		tmp += "EDGE NODES:\n";		
		tmp += F + " ;\n";
		tmp += "MOBILE DEVICE:\n";
		tmp += mobileDevice + ".\n";
		return tmp;
	}

	public void clearAll() {
		C.clear();
		F.clear();
		T.clear();
		L.clear();
		mobileDevice = null;
	}
	
}
