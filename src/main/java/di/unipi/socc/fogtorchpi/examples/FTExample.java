/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package di.unipi.socc.fogtorchpi.examples;

import java.util.ArrayList;
import di.unipi.socc.fogtorchpi.deployment.Search;
import static java.util.Arrays.asList;
import di.unipi.socc.fogtorchpi.application.Application;
import di.unipi.socc.fogtorchpi.application.ExactThing;
import di.unipi.socc.fogtorchpi.application.ThingRequirement;
import di.unipi.socc.fogtorchpi.infrastructure.Infrastructure;
import di.unipi.socc.fogtorchpi.utils.QoS;

/**
 *
 * @author stefano
 */
public class FTExample {

    /**
     * @param args the command line arguments
     */
    public Search start() {
        //Application
        Application A = FTApplication.createApplication();
        Infrastructure I = FTInfrastructure.createInfrastructure();
        
        Search s = new Search(A, I);
        s.addBusinessPolicies("mlengine", asList("cloud_2", "cloud_1"));
        s.findDeployments(true);

        return s;
    }
}
