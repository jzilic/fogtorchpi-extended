package di.unipi.socc.fogtorchpi.deployment;

import java.util.ArrayList;
import java.util.HashMap;

import at.ac.tuwien.ec.appmodel.MobileApplication;
import at.ac.tuwien.ec.appmodel.mobileapps.ChessApp;
import di.unipi.socc.fogtorchpi.application.SoftwareComponent;
import di.unipi.socc.fogtorchpi.utils.Couple;
import di.unipi.socc.fogtorchpi.utils.QoSProfile;

public class MobileWorkload extends MobileApplication {

	private ArrayList<MobileApplication> workload;
	
	
	public MobileWorkload(ArrayList<MobileApplication> workload)
	{
		this.workload = new ArrayList<MobileApplication>();
		this.workload.addAll(workload);
	}
	
	@Override
	public MobileApplication clone() {
		MobileWorkload cloned = new MobileWorkload(this.workload);
		cloned.S.clear();
		cloned.L.clear();
		cloned.S = (ArrayList<SoftwareComponent>) this.S.clone();
		cloned.L = (HashMap<Couple, QoSProfile>) this.L.clone();
		return cloned;
	}

	@Override
	public void sampleLinks() {
		this.L.clear();
		for(int i = 0; i < workload.size() - 1; i++)
		{
			MobileApplication currApp = workload.get(i);
			currApp.sampleLinks();
			for(Couple c : currApp.L.keySet())
				this.L.put(c, currApp.L.get(c));
			int lastIdx = currApp.S.size()-1;
			MobileApplication nextApp = workload.get(i+1);
			String id1 = currApp.S.get(lastIdx).getId();
			String id2 = nextApp.S.get(0).getId();
			addLink(id1,id2,Integer.MAX_VALUE,0.0);
		}
		workload.get(workload.size()-1).sampleLinks();
		for(Couple c : workload.get(workload.size()-1).L.keySet())
			this.L.put(c, workload.get(workload.size()-1).L.get(c));
		
	}

	@Override
	public void sampleTasks() {
		this.S.clear();
		for(int i = 0; i < workload.size(); i++){
			MobileApplication app = workload.get(i);
			this.S.addAll(app.S);
		}
	}

}
