package di.unipi.socc.fogtorchpi.deployment;

import di.unipi.socc.fogtorchpi.application.Application;
import di.unipi.socc.fogtorchpi.application.SoftwareComponent;
import di.unipi.socc.fogtorchpi.infrastructure.FogNode;
import di.unipi.socc.fogtorchpi.infrastructure.Infrastructure;
import di.unipi.socc.fogtorchpi.utils.Couple;
import di.unipi.socc.fogtorchpi.utils.QoSProfile;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.logging.Level;

import static java.util.Arrays.asList;

import java.util.ArrayList;

import at.ac.tuwien.ec.algorithms.BacktrackingExhaustiveResearch;
import at.ac.tuwien.ec.algorithms.FirstFitResearch;
import at.ac.tuwien.ec.algorithms.HEFTResearch;
import at.ac.tuwien.ec.algorithms.HeuristicResearch;
import at.ac.tuwien.ec.algorithms.WeightedFunctionResearch;
import at.ac.tuwien.ec.appmodel.MobileApplication;
import at.ac.tuwien.ec.appmodel.MobileSoftwareComponent;
import at.ac.tuwien.ec.edgeoffload.Helper;
import at.ac.tuwien.ec.edgeoffload.Main;
import at.ac.tuwien.ec.edgeoffload.SimulationConstants;
import at.ac.tuwien.ec.infrastructuremodel.MobileCloudInfrastructure;
import at.ac.tuwien.ec.utils.DeploymentsHistogram;
import at.ac.tuwien.ec.utils.ExponentialDistributionGenerator;
import at.ac.tuwien.ec.utils.WorkloadSearch;

/**
 *
 * @author Stefano
 */
public class MonteCarloSimulation {

    private int times;
    private MobileCloudInfrastructure I;
    private ArrayList<MobileApplication> workload;
    private HashMap<String, HashSet<String>> businessPolicies;
    private int[] workloadRuns;

    public MonteCarloSimulation(
            int times,
            Application A,
            Infrastructure I) {

        this.times = times;
        this.workload = new ArrayList<MobileApplication>();
        workload.add((MobileApplication) A);
        this.I = (MobileCloudInfrastructure) I;
        businessPolicies = new HashMap<String, HashSet<String>>();
    }
      
    public MonteCarloSimulation(
            int times,
            MobileApplication A,
            MobileCloudInfrastructure I) {

        this.times = times;
        this.workload = new ArrayList<MobileApplication>();
        workload.add((MobileApplication) A);
        this.I = I;
        businessPolicies = new HashMap<String, HashSet<String>>();
    }
    
    public MonteCarloSimulation(
            int times,
            ArrayList<MobileApplication> workload,
            MobileCloudInfrastructure I) {
        this.times = times;
        this.workload = workload;
        this.I = I;
        businessPolicies = new HashMap<String, HashSet<String>>();
    }
    
    public void addBusinessPolicies(String component, List<String> allowedNodes) {
        HashSet<String> policies = new HashSet<>();
        policies.addAll(allowedNodes);
        this.businessPolicies.put(component, policies);
    }

    private void samplingPhase(MobileCloudInfrastructure I, MobileApplication A) {
		I.clearAll();
    	Helper.setupInfrastructure(I);
		I.sample();
		A.sample();
	}
    
    public HashMap<String,DeploymentsHistogram> startSimulation(int[] appReps, List<String> fogNodes) {
    	//HashMap<Deployment, Couple<Double, Double>> histogram = new HashMap<>();
    	HashMap<String,DeploymentsHistogram> histograms = new HashMap<String,DeploymentsHistogram>();
    	//DeploymentsHistogram histogram = new DeploymentsHistogram();

    	for(String s: SimulationConstants.algorithms)
    		histograms.put(s, new DeploymentsHistogram());
    	/*histograms.put("weighted", new DeploymentsHistogram());
    	histograms.put("heft", new DeploymentsHistogram());
    	histograms.put("mincost", new DeploymentsHistogram());
    	histograms.put("maxbatt", new DeploymentsHistogram());
    	histograms.put("minmin",new DeploymentsHistogram());*/
    	
    	WorkloadSearch search = new WorkloadSearch(workload);

    	SimulationConstants.logger.setLevel(SimulationConstants.logLevel);

    	for (int j = 0; j < times; j++) {
    		MobileApplication A = extractWorkload(workload);
    		samplingPhase(I, A);
    		SimulationConstants.logger.info("Iteration "+ j);
    		//SimulationConstants.workload_runs = (int) Math.floor(ExponentialDistributionGenerator.getNext(SimulationConstants.workload_runs_lambda));
    		if(j%100 == 0)
    		{
    			System.gc();
    			if(!SimulationConstants.batch)
    				System.out.println("Iteration " + j);
    		}
    		for(String algorithm : histograms.keySet()){
    			ArrayList<Deployment> depList = search.findDeployments(I, A, algorithm);
    			double pos = depList.size();
    			double size = depList.size();

    			for (Deployment d : depList) {
    				if (histograms.get(algorithm).containsKey(d)) {
    					//Double newCount = histogram.get(d).getA() + 1.0; //montecarlo frequency
    					//Double newPos = histogram.get(d).getB() + ((pos + 1)/ (size+1));
    					double scoreUpdate = ((pos + 1)/ (size+1));
    					histograms.get(algorithm).update(d, scoreUpdate);
    				} else {
    					//histogram.put(d, new Couple(1.0, pos / (size + 1)));
    					histograms.get(algorithm).add(d, pos / (size+1));
    				}
    				pos--;
    			}
    			//System.out.println(I);
    		}
    	}
    	return histograms;
    }
    
    public HashMap<String,DeploymentsHistogram> startSimulation(List<String> fogNodes) {
    	//HashMap<Deployment, Couple<Double, Double>> histogram = new HashMap<>();
    	HashMap<String,DeploymentsHistogram> histograms = new HashMap<String,DeploymentsHistogram>();
    	//DeploymentsHistogram histogram = new DeploymentsHistogram();

    	for(int i = 0; i < SimulationConstants.algorithms.length; i++)
    		histograms.put(SimulationConstants.algorithms[i], new DeploymentsHistogram());
    	/*histograms.put("weighted", new DeploymentsHistogram());
    	histograms.put("heft", new DeploymentsHistogram());
    	histograms.put("mincost", new DeploymentsHistogram());
    	histograms.put("maxbatt", new DeploymentsHistogram());
    	histograms.put("minmin",new DeploymentsHistogram());*/

    	WorkloadSearch search = new WorkloadSearch(workload);

    	SimulationConstants.logger.setLevel(SimulationConstants.logLevel);

    	for (int j = 0; j < times; j++) {
    		MobileApplication A = extractWorkload(workload);
    		samplingPhase(I, A);
    		SimulationConstants.logger.info("Iteration "+ j);
    		//SimulationConstants.workload_runs = (int) Math.floor(ExponentialDistributionGenerator.getNext(SimulationConstants.workload_runs_lambda));
    		if(j%100 == 0)
    		{
    			System.gc();
    			if(!SimulationConstants.batch)
    				System.out.println("Iteration " + j);
    		}
    		for(String algorithm : histograms.keySet()){
    			ArrayList<Deployment> depList = search.findDeployments(I, A, algorithm);
    			double pos = depList.size();
    			double size = depList.size();

    			for (Deployment d : depList) {
    				if (histograms.get(algorithm).containsKey(d)) {
    					//Double newCount = histogram.get(d).getA() + 1.0; //montecarlo frequency
    					//Double newPos = histogram.get(d).getB() + ((pos + 1)/ (size+1));
    					double scoreUpdate = ((pos + 1)/ (size+1));
    					histograms.get(algorithm).update(d, scoreUpdate);
    				} else {
    					//histogram.put(d, new Couple(1.0, pos / (size + 1)));
    					histograms.get(algorithm).add(d, pos / (size+1));
    				}
    				pos--;
    			}
    			//System.out.println(I);
    		}
    	}
    	return histograms;
    }
	
    public MobileApplication extractWorkload(ArrayList<MobileApplication> workload){
    	MobileWorkload mWorkload = new MobileWorkload(workload);
    	
    	return mWorkload;
    }
}
