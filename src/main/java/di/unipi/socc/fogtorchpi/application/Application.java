package di.unipi.socc.fogtorchpi.application;

import di.unipi.socc.fogtorchpi.utils.Couple;
import di.unipi.socc.fogtorchpi.utils.Hardware;
import di.unipi.socc.fogtorchpi.utils.QoSProfile;
import di.unipi.socc.fogtorchpi.application.SoftwareComponent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import at.ac.tuwien.ec.appmodel.MobileSoftwareComponent;


/**
 *
 * @author stefano
 */
public class Application {
    public ArrayList<SoftwareComponent> S;
    public HashMap<Couple, QoSProfile> L;
    
    public Application(){
        S = new ArrayList<>();
        L = new HashMap<>();
    }

    public void addLink(String a, String b, int latency, double bandwidth) {
        L.put(new Couple(a,b), new QoSProfile(latency,bandwidth));
        //L.put(new Couple(b,a), new QoSProfile(latency,bandwidth));
    }

    public void addLink(String a, String b, int latency, double downlink, double uplink) {
        L.put(new Couple(a,b), new QoSProfile(latency,uplink));
        //L.put(new Couple(b,a), new QoSProfile(latency,downlink));
    }

    public void addComponent(String id, List<String> softwareReqs, Hardware hardwareReqs, ArrayList<ThingRequirement> Theta) {
        S.add(new SoftwareComponent(id, softwareReqs, hardwareReqs, Theta));   
    }
    
    public void addComponent(String id, List<String> softwareReqs, Hardware hardwareReqs) {
        S.add(new SoftwareComponent(id, softwareReqs, hardwareReqs, new ArrayList<>()));   
    }
    
    public void removeComponent(SoftwareComponent sc){
    	S.remove(sc);
    }
    
    public void removeLink(Couple link){
    	L.remove(link);
    }
    
    public ArrayList<Couple<String,String>> incidentEdgesIn(SoftwareComponent sc){
    	ArrayList<Couple<String,String>> incidentEdges = new ArrayList<Couple<String,String>>();
    	for(SoftwareComponent component: S){
    		if(sc.getId().equals(component.getId()))
    			continue;
    		Couple<String,String> edge 
    			= new Couple<String,String>(component.getId(),sc.getId());
    		if(L.containsKey(edge))
    			incidentEdges.add(edge);
    	}
    	return incidentEdges;
    }
    
    public ArrayList<Couple<String,String>> outgoingEdgesFrom(SoftwareComponent sc){
    	ArrayList<Couple<String,String>> outgoingEdges = new ArrayList<Couple<String,String>>();
    	for(SoftwareComponent component: S){
    		if(sc.getId().equals(component.getId()))
    			continue;
    		Couple<String,String> edge 
    			= new Couple<String,String>(sc.getId(),component.getId());
    		if(L.containsKey(edge))
    			outgoingEdges.add(edge);
    	}
    	return outgoingEdges;
    }
    
    @Override
    public String toString(){
        String result = "S = {\n";
        
        for (SoftwareComponent s: S){
            result+="\t"+s;
            result+="\n";
        }
        
        result+="}\n\nLambda = {\n";
        
        for (Couple c : L.keySet()){
            result+="(" + c + "\t"+ L.get(c) + ")";
            result+="\n";
        }
        
        result+="}";
        
        return result;
    }
    

}
