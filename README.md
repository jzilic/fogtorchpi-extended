This simulator is an extension of FogTorchPI simulator. The original FogTorchPI project can be found at the following link: https://github.com/di-unipi-socc/FogTorchPI.
For License information, see individual packages source directories.

Our simulator targets mobile computation offloading.